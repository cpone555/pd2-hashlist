import os
from file_path_checker import *

from config import *

UsefulExtractFileTypes = [
	".material_config",
	".object",
	".unit",
	".scene",
	".effect",
	".gui"
]

HashlistFile = open(OriginalHashlistLocation, "r")
HashlistContents = [line.rstrip() for line in HashlistFile]
HashlistFile.close()

NewHashlistContents = []

for Path, SubDirectories, Files in os.walk( PD2ExtractFolder ):
	for FileName in Files:
		if FileName.endswith(tuple(UsefulExtractFileTypes)):
			f = os.path.join(Path, FileName)
			FullPath = ( str(f) + os.linesep )[:-2]

			print("Checking: " + FullPath)

			FilePaths = CheckForFilePaths( FullPath )
			for FilePath in FilePaths:
				if ( FilePath not in HashlistContents ) and ( FilePath not in NewHashlistContents ):
					NewHashlistContents.append(FilePath)

OverallHashList = HashlistContents + NewHashlistContents

NewHashlistFile = open(NewHashlistLocation, "w")

for Hash in OverallHashList:
	NewHashlistFile.write("%s\n" % Hash)