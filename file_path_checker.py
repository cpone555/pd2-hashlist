import re
FilePathRegex = re.compile(r"\"(?:[\w]+\/)\/*.[^\s]*\"")

def CheckForFilePaths( file_path ):
	File = open( file_path, "r" )
	FileContents = File.read()

	FilePaths = []
	FileMatches = FilePathRegex.findall( FileContents )
	for Path in FileMatches:
		FilePaths.append( Path[1:-1] )

	return FilePaths